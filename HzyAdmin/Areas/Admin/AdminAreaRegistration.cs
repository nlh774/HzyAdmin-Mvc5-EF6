﻿using System.Web.Mvc;

namespace HzyAdmin.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] {
                    "HzyAdmin.Areas."+AreaName+".Controllers",
                    "HzyAdmin.Areas."+AreaName+".Controllers.Sys",
                    "HzyAdmin.Areas."+AreaName+".Controllers.Base"
                }
            );
        }
    }
}