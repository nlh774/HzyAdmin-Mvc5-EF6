﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using Models.SysClass;
using Logic.SysClass;
using Common;
using Common.ValidateHelper;

namespace HzyAdmin.Areas.Admin.Controllers
{
    [AopActionFilter(false)]
    public class LoginController : BaseController
    {
        //
        // GET: /Admin/Login/
        public AccountLogic _AccountLogic { get; set; }

        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        public override ActionResult Index()
        {
            Tools.SetSession("Account", new Account());
            return View();
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Checked(string uName, string uPwd, string loginCode)
        {
            _AccountLogic.Checked(uName, uPwd, loginCode);
            return this.Success(new
            {
                status = 1,
                jumpurl = AppConfig.HomePageUrl + "#!%u9996%u9875#!/Admin/Home/Main/"
            });
        }

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public ActionResult GetYZM()
        {
            ValidateCodeHelper vch = new ValidateCodeHelper();
            string code = vch.GetRandomNumberString(4);
            Tools.SetCookie("loginCode", code, 2);
            return File(vch.CreateImage(code), "image/jpeg");
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        public ActionResult Out()
        {
            return RedirectToAction("Index");
        }


    }
}
