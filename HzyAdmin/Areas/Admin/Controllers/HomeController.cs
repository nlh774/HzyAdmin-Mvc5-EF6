﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//
using Aop;
using Common;
using Models;
using Logic.SysClass;

namespace HzyAdmin.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Admin/Home/
        public Sys_MenuLogic _Logic { get; set; }

        protected override void Init()
        {
            base.Init();
            this.IsExecutePowerLogic = false;
        }

        public override ActionResult Index()
        {
            ViewData["MenuHtml"] = _Logic.CreateMenu();
            return View(Account);
        }

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            return View();
        }

    }
}
