using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HzyAdmin.Areas.Admin.Controllers.Sys
{
    //
    using Aop;
    using Common;
    using Models.SysClass;
    using Logic.SysClass;
    using System.Collections;

    public class ChangePwdController : BaseController
    {
        //
        // GET: /ManageSys/ChangePwd/
        public AccountLogic _Logic { get; set; }

        protected override void Init()
        {
            this.MenuID = "Z-150";
        }

        public override ActionResult Index()
        {
            ViewData["userName"] = _Logic.Get(Account.UserID).User_Name;
            return View();
        }

        [HttpPost]
        public ActionResult ChangePwd(string oldpwd, string newpwd, string newlypwd)
        {
            _Logic.ChangePwd(oldpwd, newpwd, newlypwd);
            return this.Success();
        }

    }
}
