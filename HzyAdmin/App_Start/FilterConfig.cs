﻿using System.Web;
using System.Web.Mvc;

namespace HzyAdmin
{
    using Aop;
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AopExceptionFilterAttribute(true));
        }
    }
}
