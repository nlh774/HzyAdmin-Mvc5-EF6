﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aop
{
    //引用
    using System.Web;
    using System.Web.Mvc;
    using Common;

    /// <summary>
    /// 实体 验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AopCheckEntityAttribute : ActionFilterAttribute
    {
        public AopCheckEntityAttribute()
        {

        }

        /// <summary>
        /// 每次请求Action之前发生，，在行为方法执行前执行
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var _Controller = (Controller)filterContext.Controller;
            if (!_Controller.ModelState.IsValid)
            {
                var _Keys = _Controller.ModelState.Keys;
                foreach (var key in _Keys)
                {
                    var _Error = _Controller.ModelState[key].Errors[0];
                    if (_Error != null && !string.IsNullOrEmpty(_Error.ErrorMessage))
                        throw new MessageBox(_Error.ErrorMessage);
                    continue;
                }
            }

        }

    }
}