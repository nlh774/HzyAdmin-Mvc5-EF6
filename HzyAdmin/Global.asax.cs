﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HzyAdmin
{
    using Aop;
    using Common;
    using Common.LogService;
    using Logic.Class;

    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            LogHelper.CreateRepository(Server.MapPath("/Log4Net/log4net.config"));

            //Autofac注入
            AutofacClass.Start();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //注册自定义视图
            ViewEngines.Engines.Clear();                    // 清除原MVC视图引擎规则
            ViewEngines.Engines.Add(new CustomViewEngine());  // 使用自定义视图引擎

            Tools.Log.WriteLog("应用程序已启动");
        }

        protected void Application_Error()
        {
            if (Server.GetLastError() != null) Tools.Log.WriteLog((Exception)Server.GetLastError().GetBaseException(), Request.UserHostAddress);
        }

        protected void Application_End()
        {
            //  在应用程序关闭时运行的代码
            Tools.Log.WriteLog("应用程序已结束");
        }

    }
}
