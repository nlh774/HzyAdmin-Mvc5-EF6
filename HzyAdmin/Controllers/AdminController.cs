﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HzyAdmin.Controllers
{
    using DbService.Repository;
    using Logic;

    public class AdminController : Controller
    {
        // GET: Admin

        //public IRepository<Sys_Function> _IRepository { get; set; }
        //public IUnitOfWork _IUnitOfWork { get; set; }

        public ActionResult Index()
        {
            //var list = _IRepository.Find().ToList();

            //var a = _IUnitOfWork.SqlQuery("select * from sys_function", null);

            //var a1 = _IUnitOfWork.SqlQuery("select * from sys_function", 1, 2);

            return RedirectToAction("Index", "Login", new { area = "Admin" });
        }
    }
}