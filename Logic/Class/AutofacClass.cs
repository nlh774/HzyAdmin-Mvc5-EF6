﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Class
{
    using Autofac;
    using Autofac.Integration.Mvc;
    using DbService;
    using Models;
    using DbService.Repository;
    using System.Reflection;
    using System.Web.Mvc;
    using IContainer = Autofac.IContainer;

    public class AutofacClass
    {
        private static IContainer _IContainer;

        public static void Start()
        {
            var _ContainerBuilder = new ContainerBuilder();


            //注册数据库基础操作 仓储和工作单元
            _ContainerBuilder.RegisterGeneric(typeof(RepositoryAchieve<>))
                .As(typeof(IRepository<>))
                .WithParameter(new NamedParameter("_DbContext", new AppDbContext()))
                .PropertiesAutowired();//泛型注入

            _ContainerBuilder.RegisterType(typeof(UnitOfWorkAchieve))
                .As(typeof(IUnitOfWork))
                .WithParameter(new NamedParameter("_DbContext", new AppDbContext()))
                .PropertiesAutowired();//类型注入

            //注册logic层
            _ContainerBuilder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).PropertiesAutowired();

            // 注册controller，使用属性注入
            _ContainerBuilder.RegisterControllers(Assembly.GetCallingAssembly()).PropertiesAutowired();

            // 注册所有的Attribute
            //_ContainerBuilder.RegisterFilterProvider();

            //注册所有的ApiControllers
            //_ContainerBuilder.RegisterApiControllers(Assembly.GetCallingAssembly()).PropertiesAutowired();

            _ContainerBuilder.RegisterModelBinders(Assembly.GetCallingAssembly());
            _ContainerBuilder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            //_ContainerBuilder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            _ContainerBuilder.RegisterSource(new ViewRegistrationSource());

            // 注册所有的Attribute
            _ContainerBuilder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            _IContainer = _ContainerBuilder.Build();

            //Set the MVC DependencyResolver
            DependencyResolver.SetResolver(new AutofacDependencyResolver(_IContainer));

            //Set the WebApi DependencyResolver
            //GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)_container);

            //_IContainer.Resolve(typeof(RepositoryAchieve<>), new NamedParameter("_DbContext", new AppDbContext()));
            //_IContainer.Resolve<UnitOfWorkAchieve>(new NamedParameter("_DbContext", new AppDbContext()));

            //_IContainer.ResolveOptional(typeof(RepositoryAchieve<>), new NamedParameter("_DbContext", new AppDbContext()));
            //_IContainer.ResolveOptional<UnitOfWorkAchieve>(new NamedParameter("_DbContext", new AppDbContext()));

        }

        /// <summary>
        /// 从容器中获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static T GetFromFac<T>()
        {
            return _IContainer.Resolve<T>();
            //   return (T)DependencyResolver.Current.GetService(typeof(T));
        }


    }
}
