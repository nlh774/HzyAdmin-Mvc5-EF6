﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Class
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Data.Common;
    using DbService;
    using DbService.QueryString.Class;
    using DbService.Repository;
    using Models.SysClass;
    using Common;
    using System.Reflection;
    using System.ComponentModel.DataAnnotations;
    using DbService.QueryString;
    using DbService.QueryString.Interface;
    using System.Linq.Expressions;

    public class BaseLogic<T> where T : class
    {
        /// <summary>
        /// 单个对象操作
        /// </summary>
        public IRepository<T> _RepositoryAchieve { get; set; }

        /// <summary>
        /// 对象操作
        /// </summary>
        public IUnitOfWork db { get; set; }

        /// <summary>
        /// 连表查询 对象
        /// </summary>
        public QueryString Select = new QueryString();

        /// <summary>
        /// 登录 信息 对象
        /// </summary>
        protected Account _Account = new Account();

        protected string ErrorMessge = "操作失败!";

        public BaseLogic()
        {
            _Account = this.GetSession<Account>("Account");
        }

        public void SetSession(string key, object value)
        {
            Tools.SetSession(key, value);
        }

        public TResult GetSession<TResult>(string key)
        {
            return Tools.GetSession<TResult>(key);
        }

        /// <summary>
        /// 将多个实体组合成为一个 字典类型
        /// </summary>
        /// <param name="di"></param>
        /// <returns></returns>
        public Dictionary<string, object> EntityToDictionary(Dictionary<string, object> di)
        {
            Dictionary<string, object> r = new Dictionary<string, object>();
            foreach (var item in di)
            {
                if (item.Value is BaseEntity)
                {
                    item.Value.GetType().GetProperties().ToList().ForEach(pi =>
                    {
                        if (pi.GetValue(item.Value, null) == null)
                            r.Add(pi.Name, null);
                        else
                        {
                            if (pi.PropertyType == typeof(DateTime))
                                r.Add(pi.Name, pi.GetValue(item.Value, null).ToDateTimeFormat("yyyy-MM-dd HH:mm:ss"));
                            else
                                r.Add(pi.Name, pi.GetValue(item.Value, null));
                        }
                    });
                }
                else
                {
                    r.Add(item.Key, item.Value);
                }
            }
            return r;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="SqlStr"></param>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="Param"></param>
        /// <param name="ArryEntity"></param>
        /// <returns></returns>
        public PagingEntity GetPagingEntity(string SqlStr, int Page, int Rows, DbParameter[] Param, params BaseEntity[] ArryEntity)
        {
            var _Query = db.SqlQuery(SqlStr, Page, Rows, Param);
            var _Table = (DataTable)_Query.Item1;
            var _Total = (int)_Query.Item2;

            var _PagingEntity = new PagingEntity();
            _PagingEntity.Table = _Table;
            _PagingEntity.Counts = _Total;
            _PagingEntity.PageCount = (_Total / Rows);
            _PagingEntity.List = _Table.ToList();

            this.SetHeaderJson(_PagingEntity, ArryEntity);

            return _PagingEntity;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Iquery"></param>
        /// <param name="Page"></param>
        /// <param name="Rows"></param>
        /// <param name="ArryEntity"></param>
        /// <returns></returns>
        public PagingEntity GetPagingEntity(IQuery Iquery, int Page, int Rows, params BaseEntity[] ArryEntity)
        {
            var _Sql = Iquery.ToSQL();

            var _DbParameter = new List<DbParameter>();
            foreach (var item in _Sql.Parameter)
            {
                _DbParameter.Add(new SqlParameter(item.Key, item.Value));
            }
            return this.GetPagingEntity(_Sql.Sql_Parameter, Page, Rows, _DbParameter.ToArray(), ArryEntity);
        }

        /// <summary>
        /// 生产表头 Json 对象
        /// </summary>
        /// <param name="_PagingEntity"></param>
        /// <param name="ArryEntity"></param>
        private void SetHeaderJson(PagingEntity _PagingEntity, params BaseEntity[] ArryEntity)
        {
            var dic = new Dictionary<string, object>();
            var list = new List<PropertyInfo>();
            var colNames = new List<Dictionary<string, string>>();
            ArryEntity.ToList().ForEach(item =>
            {
                //将所有实体里面的属性放入list中
                item.GetType().GetProperties().ToList().ForEach(p =>
                {
                    list.Add(p);
                });
            });
            foreach (DataColumn dc in _PagingEntity.Table.Columns)
            {
                dic = new Dictionary<string, object>();
                var col = new Dictionary<string, string>();
                var pro = list.Find(item => item.Name.Equals(dc.ColumnName));

                dic["field"] = dc.ColumnName;
                dic["align"] = "left";
                dic["sortable"] = true;
                if (pro == null)
                {
                    dic["title"] = dc.ColumnName;
                    dic["visible"] = !dc.ColumnName.Equals("_ukid");
                    col.Add(dc.ColumnName, dc.ColumnName);
                }
                else
                {
                    //获取有特性标记的属性【获取字段别名（中文名称）】
                    var FiledConfig = pro.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
                    if (FiledConfig != null)
                    {
                        dic["title"] = (string.IsNullOrEmpty(FiledConfig.Name) ? dc.ColumnName : FiledConfig.Name);
                        dic["visible"] = true;
                        col.Add(dc.ColumnName, dic["title"].ToStr());
                    }
                }
                _PagingEntity.ColNames.Add(col);
                _PagingEntity.ColModel.Add(dic);
            }
        }

        public T Get(object Id)
        {
            return _RepositoryAchieve.FindSingleById(Id);
        }

        public T Get(Expression<Func<T, bool>> _Where)
        {
            return _RepositoryAchieve.FindSingle(_Where);
        }



    }
}
