﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.CAttribute
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class SysNumberAttribute : Attribute
    {

        /// <summary>
        /// 编号长度
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// 字符串长度
        /// </summary>
        public char Str { get; set; }

        /// <summary>
        /// 编号标记
        /// </summary>
        /// <param name="length">长度</param>
        /// <param name="str">左边填充字符</param>
        public SysNumberAttribute(int length = 0, char str = '0')
        {
            this.Length = length;
            this.Str = str;
        }

    }
}
