namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_Number")]
    public partial class Sys_Number : SysClass.BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Number_ID { get; set; }

        [StringLength(50)]
        public string Number_Num { get; set; }

        [StringLength(50)]
        public string Number_DataBase { get; set; }

        [StringLength(50)]
        public string Number_TableName { get; set; }

        [StringLength(50)]
        public string Number_NumField { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Number_CreateTime { get; set; }
    }
}
