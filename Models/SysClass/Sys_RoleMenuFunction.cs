namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_RoleMenuFunction")]
    public partial class Sys_RoleMenuFunction : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RoleMenuFunction_ID { get; set; }

        public Guid? RoleMenuFunction_RoleID { get; set; }

        public Guid? RoleMenuFunction_FunctionID { get; set; }

        public Guid? RoleMenuFunction_MenuID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? RoleMenuFunction_CreateTime { get; set; }
    }
}
