namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_User")]
    public partial class Sys_User : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid User_ID { get; set; }

        [Display(Name = "姓名")]
        public string User_Name { get; set; }

        [Display(Name = "登录名")]
        public string User_LoginName { get; set; }

        [Display(Name = "密码")]
        public string User_Pwd { get; set; }

        [Display(Name = "邮件")]
        public string User_Email { get; set; }

        [Display(Name = "是否删除")]
        public int? User_IsDelete { get; set; }

        [Display(Name = "创建时间"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? User_CreateTime { get; set; }
    }
}
