namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_Function")]
    public partial class Sys_Function : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Function_ID { get; set; }

        [Display(Name = "编号")]
        public string Function_Num { get; set; }

        [Display(Name = "名称")]
        public string Function_Name { get; set; }

        [Display(Name = "别名")]
        public string Function_ByName { get; set; }
        
        [Display(Name = "创建时间"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Function_CreateTime { get; set; }
    }
}
