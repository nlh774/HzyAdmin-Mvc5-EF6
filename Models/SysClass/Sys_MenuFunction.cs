namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_MenuFunction")]
    public partial class Sys_MenuFunction : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MenuFunction_ID { get; set; }

        public Guid? MenuFunction_MenuID { get; set; }

        public Guid? MenuFunction_FunctionID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? MenuFunction_CreateTime { get; set; }
    }
}
