namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_UserRole")]
    public partial class Sys_UserRole : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserRole_ID { get; set; }

        public Guid? UserRole_UserID { get; set; }

        public Guid? UserRole_RoleID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? UserRole_CreateTime { get; set; }
    }
}
