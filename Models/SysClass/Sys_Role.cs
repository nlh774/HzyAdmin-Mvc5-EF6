namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_Role")]
    public partial class Sys_Role : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Role_ID { get; set; }

        [Display(Name = "编号")]
        public string Role_Num { get; set; }

        [Display(Name = "角色名称")]
        public string Role_Name { get; set; }

        [Display(Name = "备注")]
        public string Role_Remark { get; set; }

        [Display(Name = "是否删除")]
        public int? Role_IsDelete { get; set; }

        [Display(Name = "创建时间"),DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Role_CreateTime { get; set; }
    }
}
