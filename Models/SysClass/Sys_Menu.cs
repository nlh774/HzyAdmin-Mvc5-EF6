namespace Models.SysClass
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sys_Menu")]
    public partial class Sys_Menu : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Menu_ID { get; set; }

        [Display(Name = "编号")]
        public string Menu_Num { get; set; }

        [Display(Name = "名称")]
        public string Menu_Name { get; set; }

        [Display(Name = "访问路径")]
        public string Menu_Url { get; set; }

        [Display(Name = "图标")]
        public string Menu_Icon { get; set; }

        [Display(Name = "父级ID")]
        public Guid? Menu_ParentID { get; set; }

        [Display(Name = "是否显示")]
        public int? Menu_IsShow { get; set; }

        [Display(Name = "创建时间"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Menu_CreateTime { get; set; }
    }
}
