namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using Models.CAttribute;

    [Table("Member")]
    public partial class Member : SysClass.BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Member_ID { get; set; }

        [Display(Name = "编号"), SysNumber]
        public string Member_Num { get; set; }

        [Display(Name = "姓名")]
        public string Member_Name { get; set; }

        [Display(Name = "电话")]
        public int? Member_Phone { get; set; }

        [Display(Name = "性别")]
        public string Member_Sex { get; set; }

        [Display(Name = "生日")]
        public DateTime? Member_Birthday { get; set; }

        [Display(Name = "头像")]
        public string Member_Photo { get; set; }

        [Display(Name = "用户ID")]
        public Guid? Member_UserID { get; set; }

        [Display(Name = "简历")]
        public string Member_Introduce { get; set; }

        [Display(Name = "文件地址")]
        public string Member_FilePath { get; set; }

        [Display(Name = "创建时间"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Member_CreateTime { get; set; }
    }
}
