﻿/*【工作单元接口】
 * 
 * 
 * 3、项目庞大，扩展性高，有并发处理需求

因为项目涉及高并发，采用仓储模式+工作单元模式的设计，使用工作单元的原因是可以提高数据库写操作负载，并且在仓储模式中可以根据不同的数据库链接字符串读不同的库。

对于并发的，可以分为多线程、并行处理、异步编程、响应式编程。（引用：《Concurrency in C# Cookbook》—Author，Stephen Cleary）

在仓储中我会使用异步编程实现并发。
 * 
 * 
 * 
 * 
 * 
 * 
 * https://www.cnblogs.com/Zhang-Xiang/p/7839540.html
 * 
 * 
 * 
 */

using System;
using System.Linq;

namespace DbService.Repository
{
    using System.Data.Common;
    using System.Data.Entity.Infrastructure;
    using System.Linq.Expressions;
    using System.Data;

    public interface IUnitOfWork //: IDisposable
    {
        void Insert<T>(T _Entity) where T : class;

        void InsertBatch<T>(T[] _Entity) where T : class;

        void Update<T>(Expression<Func<T, T>> _Entity, Expression<Func<T, bool>> _Where) where T : class;

        void UpdateById<T>(T _Entity) where T : class;

        void Delete<T>(Expression<Func<T, bool>> _Where) where T : class;

        void Delete<T>(T _Entity) where T : class;

        void DeleteById<T>(object Id) where T : class;

        T FindSingle<T>(Expression<Func<T, bool>> _Where) where T : class;

        T FindSingleById<T>(object Id) where T : class;

        bool IsExist<T>(Expression<Func<T, bool>> _Where) where T : class;

        int GetCount<T>(Expression<Func<T, bool>> _Where) where T : class;

        IQueryable<T> Find<T>(Expression<Func<T, bool>> _Where = null) where T : class;

        int ExecuteNonQuery(string SqlString, DbParameter[] _DbParameter = null);

        object ExecuteScalar(string SqlString, DbParameter[] _DbParameter = null);

        DbRawSqlQuery<TResult> SqlQuery<TResult>(string SqlString, DbParameter[] _DbParameter = null) where TResult : class;

        DataTable SqlQuery(string SqlString, DbParameter[] _DbParameter = null);

        Tuple<DataTable, object> SqlQuery(string SqlString, int Page, int Rows, DbParameter[] _DbParameter = null);

        DataSet SqlQueryDataSet(string SqlString, DbParameter[] _DbParameter = null, Action<DbCommand> _Action = null);

        int Save();

        void Commit(Action _Action);

    }
}
