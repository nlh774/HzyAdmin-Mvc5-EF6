﻿/*【仓储接口】
 * 
 * 
 * 3、项目庞大，扩展性高，有并发处理需求

因为项目涉及高并发，采用仓储模式+工作单元模式的设计，使用工作单元的原因是可以提高数据库写操作负载，并且在仓储模式中可以根据不同的数据库链接字符串读不同的库。

对于并发的，可以分为多线程、并行处理、异步编程、响应式编程。（引用：《Concurrency in C# Cookbook》—Author，Stephen Cleary）

在仓储中我会使用异步编程实现并发。
 * 
 * 
 * 
 * 
 * 
 * 
 * https://www.cnblogs.com/Zhang-Xiang/p/7839540.html
 * 
 * 
 * 
 */

using System;
using System.Linq;
using System.Collections.Generic;

namespace DbService.Repository
{

    using System.Data.Common;
    using System.Data.Entity.Infrastructure;
    using System.Linq.Expressions;

    public interface IRepository<T> where T : class
    {
        void Insert(T _Entity);

        void InsertBatch(T[] _Entity);

        void Update(Expression<Func<T, T>> _Entity, Expression<Func<T, bool>> _Where);

        void UpdateById(T _Entity);

        void Delete(Expression<Func<T, bool>> _Where);

        void Delete(T _Entity);

        void DeleteById(object Id);

        T FindSingle(Expression<Func<T, bool>> _Where);

        T FindSingleById(object Id);

        bool IsExist(Expression<Func<T, bool>> _Where);

        int GetCount(Expression<Func<T, bool>> _Where);

        IQueryable<T> Find(Expression<Func<T, bool>> _Where = null);

        int Save();

        void Commit(Action _Action);

    }
}
