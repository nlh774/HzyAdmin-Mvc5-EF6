﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbService.QueryString.Abstract
{
    public class BaseClass
    {
        protected StringBuilder _Code { get; set; }

        public BaseClass()
        {
            _Code = new StringBuilder();
        }

    }
}
