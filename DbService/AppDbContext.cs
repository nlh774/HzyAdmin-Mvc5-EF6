namespace DbService
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Infrastructure;
    //
    using Models;
    using Models.SysClass;

    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("name=AppDb")
        {
        }

        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<Sys_Function> Sys_Function { get; set; }
        public virtual DbSet<Sys_Menu> Sys_Menu { get; set; }
        public virtual DbSet<Sys_MenuFunction> Sys_MenuFunction { get; set; }
        public virtual DbSet<Sys_Number> Sys_Number { get; set; }
        public virtual DbSet<Sys_Role> Sys_Role { get; set; }
        public virtual DbSet<Sys_RoleMenuFunction> Sys_RoleMenuFunction { get; set; }
        public virtual DbSet<Sys_User> Sys_User { get; set; }
        public virtual DbSet<Sys_UserRole> Sys_UserRole { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_Num)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_Sex)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_Photo)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_Introduce)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Member>()
            //    .Property(e => e.Member_FilePath)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Function>()
            //    .Property(e => e.Function_Num)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Function>()
            //    .Property(e => e.Function_Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Function>()
            //    .Property(e => e.Function_ByName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Menu>()
            //    .Property(e => e.Menu_Num)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Menu>()
            //    .Property(e => e.Menu_Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Menu>()
            //    .Property(e => e.Menu_Url)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Menu>()
            //    .Property(e => e.Menu_Icon)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Number>()
            //    .Property(e => e.Number_Num)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Number>()
            //    .Property(e => e.Number_DataBase)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Number>()
            //    .Property(e => e.Number_TableName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Number>()
            //    .Property(e => e.Number_NumField)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Role>()
            //    .Property(e => e.Role_Num)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Role>()
            //    .Property(e => e.Role_Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_Role>()
            //    .Property(e => e.Role_Remark)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_User>()
            //    .Property(e => e.User_Name)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_User>()
            //    .Property(e => e.User_LoginName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_User>()
            //    .Property(e => e.User_Pwd)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Sys_User>()
            //    .Property(e => e.User_Email)
            //    .IsUnicode(false);
        }




    }
}
